/* Play noughts and crosses (tic-tac-toe) between two human players. */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Constants to represent validity checking.
enum validity { Unchecked, OK, BadFormat, BadLetter, BadDigit, BadCell, Level, BadSave };
typedef enum validity validity;

// Constants to represent command inputs
enum command { Exit, Solve, Reset, Check, Save, Load, NewG };
typedef enum command command;

// Constants to represent difficulty levels for sudoku puzzle
enum diff { New, Easy, Medium, Hard, Insane, Impossible };
typedef enum diff diff;

// A game object contains the entire current state of a game. It contains the
// grid with the numbers in the sudoku puzzle, which can be updated by the user,
// a grid that contains the numbers originally fed into the program, a grid that,
// has the positions for the numbers originally fed into the program for display,
// a grid that is used to store the solution of the puzzle and a grid to check if
// the numbers input by the user match the solution of not
struct game {
    int grid[9][9];
    int staticgrid[9][9];
    int check[9][9];
    int solved[9][9];
    int comp[9][9];
};
typedef struct game game;

//-----------------------------------------------------------------------------
diff difficulty(char *text);

// Initialize the static grid for the given difficulty
void newSGrid(game *g, diff d) {
    //source: https://www.sudoku.ws/1-1.png
    if (d == New) {
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->staticgrid[i][j] = 0;
        }
      }
    }
    else if (d == Easy) {
      int game[9][9]= {{7, 9, 0, 0, 0, 0, 3, 0, 0},
                       {0, 0, 0, 0, 0, 6, 9, 0, 0},
                       {8, 0, 0, 0, 3, 0, 0, 7, 6},
                       {0, 0, 0, 0, 0, 5, 0, 0, 2},
                       {0, 0, 5, 4, 1, 8, 7, 0, 0},
                       {4, 0, 0, 7, 0, 0, 0, 0, 0},
                       {6, 1, 0, 0, 9, 0, 0, 0, 8},
                       {0, 0, 2, 3, 0, 0, 0, 0, 0},
                       {0, 0, 9, 0, 0, 0, 0, 5, 4}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->staticgrid[i][j] = game[i][j];
        }
      }
    }
    //source: http://www.puzzles.ca/sudoku_puzzles/sudoku_medium_007.gif
    else if (d == Medium) {
      int game[9][9]= {{8, 0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 6, 0, 0, 1, 2, 5},
                       {0, 4, 0, 0, 0, 0, 6, 0, 0},
                       {7, 0, 0, 0, 4, 9, 8, 0, 1},
                       {3, 6, 0, 7, 0, 0, 0, 4, 9},
                       {0, 0, 0, 0, 0, 8, 0, 5, 0},
                       {0, 2, 1, 8, 0, 5, 3, 0, 0},
                       {5, 0, 9, 0, 0, 0, 4, 0, 0},
                       {6, 3, 0, 0, 0, 2, 0, 0, 0}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->staticgrid[i][j] = game[i][j];
        }
      }
    }
    //source: https://www.sudoku.ws/3-18.png
    else if (d == Hard) {
      int game[9][9]= {{0, 1, 0, 7, 0, 6, 0, 0, 0},
                       {7, 0, 3, 0, 0, 0, 0, 0, 0},
                       {0, 2, 0, 5, 4, 0, 0, 0, 0},
                       {3, 4, 0, 0, 0, 0, 2, 5, 0},
                       {5, 0, 0, 0, 0, 0, 0, 0, 1},
                       {0, 8, 1, 0, 0, 0, 0, 7, 4},
                       {0, 0, 0, 0, 5, 7, 0, 8, 0},
                       {0, 0, 0, 0, 0, 0, 3, 0, 9},
                       {0, 0, 0, 8, 0, 1, 0, 4, 0}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->staticgrid[i][j] = game[i][j];
        }
      }
    }
    //source: https://commons.wikimedia.org/wiki/File:Sudoku_puzzle_hard_for_brute_force.jpg
    else if (d == Insane) {
      int game[9][9]= {{0, 0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 3, 0, 8, 5},
                       {0, 0, 1, 0, 2, 0, 0, 0, 0},
                       {0, 0, 0, 5, 0, 7, 0, 0, 0},
                       {0, 0, 4, 0, 0, 0, 1, 0, 0},
                       {0, 9, 0, 0, 0, 0, 0, 0, 0},
                       {5, 0, 0, 0, 0, 0, 0, 7, 3},
                       {0, 0, 2, 0, 1, 0, 0, 0, 0},
                       {0, 0, 0, 0, 4, 0, 0, 0, 9}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->staticgrid[i][j] = game[i][j];
        }
      }
    }
    //source: http://www.jibble.org/impossible-sudoku/UNSOLVABLE-1.png
    else if (d == Impossible) {
      int game[9][9]= {{0, 7, 0, 0, 0, 6, 0, 0, 0},
                       {9, 0, 0, 0, 0, 0, 0, 4, 1},
                       {0, 0, 8, 0, 0, 9, 0, 5, 0},
                       {0, 9, 0, 0, 0, 7, 0, 0, 2},
                       {0, 0, 3, 0, 0, 0, 8, 0, 0},
                       {4, 0, 0, 8, 0, 0, 0, 1, 0},
                       {0, 8, 0, 3, 0, 0, 9, 0, 0},
                       {1, 6, 0, 0, 0, 0, 0, 0, 7},
                       {0, 0, 0, 5, 0, 0, 0, 8, 0}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->staticgrid[i][j] = game[i][j];
        }
      }
    }
}

//Initialize a game, given the difficulty
void newGame(game *g, diff d) {
    //source: https://www.sudoku.ws/1-1.png
    if (d == New) {
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->grid[i][j] = 0;
          g->solved[i][j] = 0;
        }
      }
    }
    else if (d == Easy) {
      int game[9][9]= {{7, 9, 0, 0, 0, 0, 3, 0, 0},
                       {0, 0, 0, 0, 0, 6, 9, 0, 0},
                       {8, 0, 0, 0, 3, 0, 0, 7, 6},
                       {0, 0, 0, 0, 0, 5, 0, 0, 2},
                       {0, 0, 5, 4, 1, 8, 7, 0, 0},
                       {4, 0, 0, 7, 0, 0, 0, 0, 0},
                       {6, 1, 0, 0, 9, 0, 0, 0, 8},
                       {0, 0, 2, 3, 0, 0, 0, 0, 0},
                       {0, 0, 9, 0, 0, 0, 0, 5, 4}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->grid[i][j] = game[i][j];
          g->solved[i][j] = game[i][j];
        }
      }
    }
    //source: http://www.puzzles.ca/sudoku_puzzles/sudoku_medium_007.gif
    else if (d == Medium) {
      int game[9][9]= {{8, 0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 6, 0, 0, 1, 2, 5},
                       {0, 4, 0, 0, 0, 0, 6, 0, 0},
                       {7, 0, 0, 0, 4, 9, 8, 0, 1},
                       {3, 6, 0, 7, 0, 0, 0, 4, 9},
                       {0, 0, 0, 0, 0, 8, 0, 5, 0},
                       {0, 2, 1, 8, 0, 5, 3, 0, 0},
                       {5, 0, 9, 0, 0, 0, 4, 0, 0},
                       {6, 3, 0, 0, 0, 2, 0, 0, 0}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->grid[i][j] = game[i][j];
          g->solved[i][j] = game[i][j];
        }
      }
    }
    //source: https://www.sudoku.ws/3-18.png
    else if (d == Hard) {
      int game[9][9]= {{0, 1, 0, 7, 0, 6, 0, 0, 0},
                       {7, 0, 3, 0, 0, 0, 0, 0, 0},
                       {0, 2, 0, 5, 4, 0, 0, 0, 0},
                       {3, 4, 0, 0, 0, 0, 2, 5, 0},
                       {5, 0, 0, 0, 0, 0, 0, 0, 1},
                       {0, 8, 1, 0, 0, 0, 0, 7, 4},
                       {0, 0, 0, 0, 5, 7, 0, 8, 0},
                       {0, 0, 0, 0, 0, 0, 3, 0, 9},
                       {0, 0, 0, 8, 0, 1, 0, 4, 0}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->grid[i][j] = game[i][j];
          g->solved[i][j] = game[i][j];
        }
      }
    }
    //source: https://commons.wikimedia.org/wiki/File:Sudoku_puzzle_hard_for_brute_force.jpg
    else if (d == Insane) {
      int game[9][9]= {{0, 0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 0, 0, 0, 3, 0, 8, 5},
                       {0, 0, 1, 0, 2, 0, 0, 0, 0},
                       {0, 0, 0, 5, 0, 7, 0, 0, 0},
                       {0, 0, 4, 0, 0, 0, 1, 0, 0},
                       {0, 9, 0, 0, 0, 0, 0, 0, 0},
                       {5, 0, 0, 0, 0, 0, 0, 7, 3},
                       {0, 0, 2, 0, 1, 0, 0, 0, 0},
                       {0, 0, 0, 0, 4, 0, 0, 0, 9}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->grid[i][j] = game[i][j];
          g->solved[i][j] = game[i][j];
        }
      }
    }
    //source: http://www.jibble.org/impossible-sudoku/UNSOLVABLE-1.png
    else if (d == Impossible) {
      int game[9][9]= {{0, 7, 0, 0, 0, 6, 0, 0, 0},
                       {9, 0, 0, 0, 0, 0, 0, 4, 1},
                       {0, 0, 8, 0, 0, 9, 0, 5, 0},
                       {0, 9, 0, 0, 0, 7, 0, 0, 2},
                       {0, 0, 3, 0, 0, 0, 8, 0, 0},
                       {4, 0, 0, 8, 0, 0, 0, 1, 0},
                       {0, 8, 0, 3, 0, 0, 9, 0, 0},
                       {1, 6, 0, 0, 0, 0, 0, 0, 7},
                       {0, 0, 0, 5, 0, 0, 0, 8, 0}};
      for (size_t i = 0; i < 9; i++) {
        for (size_t j = 0; j < 9; j++) {
          g->grid[i][j] = game[i][j];
          g->solved[i][j] = game[i][j];
        }
      }
    }
}

// Check whether a square is already given or not.
void fsquare(game *g) {
    for (size_t i = 0; i < 9; i++) {
      for (size_t j = 0; j < 9; j++) {
        if (g->staticgrid[i][j] > 0) g->check[i][j] = 1;
        else g->check[i][j] = 0;
        //printf("%d ", g->check[i][j]);
      }
      //printf("\n");
    }
}

// Sudoku Solver, source: https://codereview.stackexchange.com/questions/37430/sudoku-solver-in-c
bool vsquare(game *g, int r, int c, int m) {
    int rbox = (r/3) * 3;
    int cbox = (c/3) * 3;
    int i;
    for (i = 0; i < 9; i++) {
      if (g->solved[r][i] == m) return 0;
      if (g->solved[i][c] == m) return 0;
      if (g->solved[rbox + (i%3)][cbox + (i/3)] == m) return 0;
    }
    return 1;
}

int solve(game *g, int r, int c) {
    int num;
    if ((r < 9) && (c < 9)) {
      if (g->solved[r][c] != 0) {
        if ((c + 1) < 9) return solve(g, r, c + 1);
        else if ((r + 1) < 9) return solve(g, r + 1, 0);
        else return 1;
      }
      else {
        for (num = 0; num < 9; num++) {
          if (vsquare(g, r, c, num+1)) {
            g->solved[r][c] = num+1;
            if ((c + 1) < 9) {
              if (solve(g, r, c + 1)) return 1;
              else g->solved[r][c] = 0;
            }
            else if ((r + 1) < 9) {
              if (solve(g, r + 1, 0)) return 1;
              else g->solved[r][c] = 0;
            }
            else return 1;
          }
        }
      }
      return 0;
    }
    else return 1;
}

// Checks if the numbers input by the users are correct or not, by comparing them
// to the solution
void checkGame(game *g){
    solve(g, 0, 0);
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        if((g->grid[i][j] == g->solved[i][j]) && (g->check[i][j] != 1)) g->comp[i][j] = 1;
        else g->comp[i][j] = 0;
      }
    }
}

// Function to create a save file using the name given by the user
void savefile(game *g, char *text, diff d) {
    int count = strlen(text) + 5;
    text[count - 6] = '\0';
    char txt[count];
    txt[0] = '\0';
    char ext[5] = ".txt";
    strcat(txt, text);
    strcat(txt, ext);
    //printf("%s\n", txt);
    FILE *out = fopen(txt, "w");
    //fprintf(out, "\r\n");
    for (size_t i = 0; i < 9; i++) {
      for (size_t j = 0; j < 9; j++) {
        fprintf(out, "%d", g->grid[i][j]);
      }
      //fprintf(out, "\r\n");
    }
    if(d == Easy) fprintf(out, "Easy");
    if(d == Medium) fprintf(out, "Medium");
    if(d == Hard) fprintf(out, "Hard");
    if(d == Insane) fprintf(out, "Insane");
    fclose(out);
}

// Check if the save file provided by the user is valid or not
bool invalidsave(game *g) {
  for (size_t i = 0; i < 9; i++) {
    for (size_t j = 0; j < 9; j++) {
      if ((g->grid[i][j] > 9) || (g->grid[i][j] < 0)) {
        return 1;
        break;
      }
    }
  }
  return 0;
}

// Convert the letter in a valid move string such as "b2" into a row index.
int row(char *text) {
    if (tolower(text[0]) == 'a') return 0;
    else if (tolower(text[0]) == 'b') return 1;
    else if (tolower(text[0]) == 'c') return 2;
    else if (tolower(text[0]) == 'd') return 3;
    else if (tolower(text[0]) == 'e') return 4;
    else if (tolower(text[0]) == 'f') return 5;
    else if (tolower(text[0]) == 'g') return 6;
    else if (tolower(text[0]) == 'h') return 7;
    else if (tolower(text[0]) == 'i') return 8;
    else return -1;
}

// Convert the digit in a valid move string such as "b2" into a column index.
int col(char *text) {
    if (tolower(text[1]) == '1') return 0;
    else if (tolower(text[1]) == '2') return 1;
    else if (tolower(text[1]) == '3') return 2;
    else if (tolower(text[1]) == '4') return 3;
    else if (tolower(text[1]) == '5') return 4;
    else if (tolower(text[1]) == '6') return 5;
    else if (tolower(text[1]) == '7') return 6;
    else if (tolower(text[1]) == '8') return 7;
    else if (tolower(text[1]) == '9') return 8;
    else return -1;
}

// Check if the input by the user is "level"
bool lvl(char *text) {
  char txt[6];
  for (size_t i = 0; i < 6; i++) {
    txt[i] = tolower(text[i]);
  }
  txt[5] = '\0';
  char level[6] = "level";
  if (strcmp(level, txt) == 0) return 1;
  else return 0;
}

// Check whether a move string typed in by the user such as "b2" is valid, and
// the corresponding cell is available.  Return a validity code.
validity valid(game *g, char *text) {
    int length = strlen(text);
    if (lvl(text) == 1) return Level;
    else if (invalidsave(g) == 1) return BadSave;
    else if ((length >= 6) || (length < 3)) return BadFormat;
    else if (row(text) == -1) return BadLetter;
    else if (col(text) == -1) return BadDigit;
    else if (g->check[row(text)][col(text)] == 1) return BadCell;
    else return OK;
}

// Convert the digit in a character such as '4' into a number.
int mov(char num) {
  if (num == '0') return 0;
  else if (num == '1') return 1;
  else if (num == '2') return 2;
  else if (num == '3') return 3;
  else if (num == '4') return 4;
  else if (num == '5') return 5;
  else if (num == '6') return 6;
  else if (num == '7') return 7;
  else if (num == '8') return 8;
  else if (num == '9') return 9;
  else return -1;
}

// Check if the input is "quit" or "exit".
bool quit(char *text) {
    int x = strlen(text) - 1;
    char txt[5];
    for (size_t i = 0; i < 5; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[4] = '\0';
    char quit[5] = "quit";
    char ext[5] = "exit";
    int y = strlen(txt);
    if ((strcmp(quit, txt) == 0) && (x == y)) return 1;
    else if ((strcmp(ext, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "reset"
bool reset(char *text) {
    int x = strlen(text) - 1;
    char txt[6];
    for (size_t i = 0; i < 6; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[5] = '\0';
    char reset[6] = "reset";
    char clear[6] = "clear";
    int y = strlen(txt);
    if ((strcmp(reset, txt) == 0) && (x == y)) return 1;
    else if ((strcmp(clear, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "solve"
bool issolve(char *text) {
    int x = strlen(text) - 1;
    char txt[6];
    for (size_t i = 0; i < 6; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[5] = '\0';
    int y = strlen(txt);
    char solve[6] = "solve";
    if ((strcmp(solve, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "check"
bool check(char *text) {
    int x = strlen(text) - 1;
    char txt[6];
    for (size_t i = 0; i < 6; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[5] = '\0';
    int y = strlen(txt);
    char check[6] = "check";
    if ((strcmp(check, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "save"
bool save(char *text) {
    int x = strlen(text) - 1;
    char txt[5];
    for (size_t i = 0; i < 5; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[4] = '\0';
    char save[5] = "save";
    int y = strlen(txt);
    if ((strcmp(save, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "level"
bool load(char *text) {
    int x = strlen(text) - 1;
    char txt[5];
    for (size_t i = 0; i < 5; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[4] = '\0';
    char load[5] = "load";
    int y = strlen(txt);
    if ((strcmp(load, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "new"
bool new(char *text) {
    int x = strlen(text) - 1;
    char txt[4];
    for (size_t i = 0; i < 4; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[3] = '\0';
    int y = strlen(txt);
    char new[4] = "new";
    if ((strcmp(new, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Return a corresponding code depending on the input
command cmd(char *text){
  if (quit(text) == 1) return Exit;
  else if (reset(text) == 1) return Reset;
  else if (issolve(text) == 1) return Solve;
  else if (check(text) == 1) return Check;
  else if (save(text) == 1) return Save;
  else if (load(text) == 1) return Load;
  else if (new(text) == 1) return NewG;
  else return -1;
}

// Check if the input by the user is "easy"
bool easy(char *text) {
    int x = strlen(text) - 1;
    char txt[5];
    for (size_t i = 0; i < 5; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[4] = '\0';
    int y = strlen(txt);
    char easy[5] = "easy";
    if ((strcmp(easy, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "medium"
bool medium(char *text) {
    int x = strlen(text) - 1;
    char txt[7];
    for (size_t i = 0; i < 7; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[6] = '\0';
    int y = strlen(txt);
    char medium[7] = "medium";
    if ((strcmp(medium, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "hard"
bool hard(char *text) {
    int x = strlen(text) - 1;
    char txt[5];
    for (size_t i = 0; i < 5; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[4] = '\0';
    int y = strlen(txt);
    char hard[5] = "hard";
    if ((strcmp(hard, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Check if the input by the user is "insane"
bool insane(char *text) {
    int x = strlen(text) - 1;
    char txt[7];
    for (size_t i = 0; i < 7; i++) {
      txt[i] = tolower(text[i]);
    }
    txt[6] = '\0';
    int y = strlen(txt);
    char insane[7] = "insane";
    if ((strcmp(insane, txt) == 0) && (x == y)) return 1;
    else return 0;
}

// Return a corresponding difficulty depending on the input
diff difficulty(char *text){
    if (easy(text) == 1) return Easy;
    else if (medium(text) == 1) return Medium;
    else if (hard(text) == 1) return Hard;
    else if (insane(text) == 1) return Insane;
    else return -1;
}

// Make a move at the given valid position.
void move(game *g, int r, int c, int m) {
    g->grid[r][c] = m;
}

// Check if the given character is a digit
bool digit(char c) {
    if (isdigit(c) == 0) return 0;
    else return 1;
}

// Load a .txt file using the filename provided by the user
void loadfile(game *g, char *text) {
    int count = strlen(text) + 5;
    text[count - 6] = '\0';
    char txt[count];
    txt[0] = '\0';
    char ext[5] = ".txt";
    strcat(txt, text);
    strcat(txt, ext);
    //printf("%s\n", txt);
    newGame(g, New);
    FILE *in = fopen(txt, "r");
    if (in) {
      char number = fgetc(in);
      int x = number - '0';
      int i = 0;
      int j = 0;
      while(! feof(in)) {
        if (digit(number) == 1){
          //printf("i = %d | j = %d | x = %d\n", i, j, x);
          move(g, i, j, x);
          j = j + 1;
          j = j % 9;
          if (j == 0) i = i + 1;
          i = i % 9;
          number = fgetc(in);
          x = number - '0';
        }
        else {
          number = fgetc(in);
        }
      }
      fclose(in);
    }
    else {
      printf(ANSI_COLOR_RED "The file " ANSI_COLOR_RESET "%s" ANSI_COLOR_RED " does not exist\n" ANSI_COLOR_RESET, txt);
      exit(0);
    }
}

// Find out which difficulty puzzle it is using the data in the .txt save file
diff loadlevel(game *g, char *text) {
    int count = strlen(text) + 5;
    text[count - 5] = '\0';
    char txt[count];
    txt[0] = '\0';
    char ext[5] = ".txt";
    strcat(txt, text);
    strcat(txt, ext);
    //printf("%s\n", txt);
    //newGame(g, New);
    //printf("%s\n", txt);
    FILE *in = fopen(txt, "r");
    char x = fgetc(in);
    char y[10];
    int z = 0;
    while(! feof(in)) {
      if(digit(x) == 0) {
        y[z] = x;
        z = z+1;
        x = fgetc(in);
      }
      else {
        x = fgetc(in);
      }
    }
    y[z] = '\0';
    //printf("%s\n", y);
    fclose(in);
    for (size_t i = 0; i < z; i++) {
      y[i] = tolower(y[i]);
    }
    if (strcmp("easy", y) == 0) return Easy;
    else if (strcmp("medium", y) == 0) return Medium;
    else if (strcmp("hard", y) == 0) return Hard;
    else if (strcmp("insane", y) == 0) return Insane;
    else return -1;
}

//-----------------------------------------------------------------------------

// Print a validity error message.
void printInvalid(validity v) {
    if (v == BadFormat) printf(ANSI_COLOR_RED "Type row and column, and then the number you want to input. E.g: c4 7. For more detail, check the readme file" ANSI_COLOR_RESET);
    else if (v == BadLetter) printf(ANSI_COLOR_RED "Type a letter between a and i, for more detail, check the readme file" ANSI_COLOR_RESET);
    else if (v == BadDigit) printf(ANSI_COLOR_RED "Type a digit between 1 and 9, for more detail, check the readme file" ANSI_COLOR_RESET);
    else if (v == BadCell) printf(ANSI_COLOR_RED "Choose an empty cell" ANSI_COLOR_RESET);
    else if (v == BadSave) printf(ANSI_COLOR_RED "The save file could not be loaded" ANSI_COLOR_RESET);
    printf("\n");
}

// Print a corresponding command message
void printCommand(command c) {
    if (c == Exit) printf("Exiting...");
    else if (c == Solve) printf("Solving...");
    else if (c == Reset) printf("Resetting...");
    else if (c == Check) printf("Checking...");
    else if (c == Save) printf("Saving...");
    else if (c == Load) printf("Loading...");
    printf("\n");
}

// Print the difficulty level being loaded
void printLevel(diff d) {
    if (d == Easy) printf(ANSI_COLOR_YELLOW "Loading... Difficulty: Easy" ANSI_COLOR_RESET);
    else if (d == Medium) printf(ANSI_COLOR_YELLOW "Loading... Difficulty: Medium" ANSI_COLOR_RESET);
    else if (d == Hard) printf(ANSI_COLOR_YELLOW "Loading... Difficulty: Hard" ANSI_COLOR_RESET);
    else if (d == Insane) printf(ANSI_COLOR_YELLOW "Loading... Difficulty: Insane" ANSI_COLOR_RESET);
    else printf(ANSI_COLOR_RED "Available difficulties are: Easy, Medium, Hard, Insane" ANSI_COLOR_RESET);
    printf("\n");
}

// Display the grid.
void display(game *g) {
    char a = 'a';
    //printf("\n");
    printf("       1   2   3   4   5   6   7   8   9\n");
    for (size_t i = 0; i < 9; i++) {
      printf("    --------------------------------------\n");
      printf(" %c", a);
      for (size_t j = 0; j < 9; j++) {
        int x = g->grid[i][j];
        int y = g->check[i][j];
        if ((j == 0) && (x != 0) && (y == 1)) printf("   | " ANSI_COLOR_CYAN "%d " ANSI_COLOR_RESET, x);
        else if ((j == 0) && (x != 0) && (y == 0)) printf("   | %d ", x);
        else if ((j == 0) && (x == 0)) printf("   |   ");
        else if ((j == 8) && (x != 0) && (y == 1)) printf("| " ANSI_COLOR_CYAN "%d" ANSI_COLOR_RESET " |\n", x);
        else if ((j == 8) && (x != 0) && (y == 0)) printf("| %d |\n", x);
        else if ((j == 8) && (x == 0)) printf("|   |\n");
        else if (x == 0) printf ("|   ");
        else if (y == 1) printf("| " ANSI_COLOR_CYAN "%d " ANSI_COLOR_RESET, x);
        else printf("| %d ", x);
      }
      a++;
    }
    printf("    --------------------------------------\n");
}

// Display the solved puzzle
void displaysolved(game *g) {
    char a = 'a';
    //printf("\n");
    printf("       1   2   3   4   5   6   7   8   9\n");
    for (size_t i = 0; i < 9; i++) {
      printf("    --------------------------------------\n");
      printf(" %c", a);
      for (size_t j = 0; j < 9; j++) {
        int x = g->solved[i][j];
        int y = g->check[i][j];
        if ((j == 0) && (y == 1)) printf("   | " ANSI_COLOR_CYAN "%d " ANSI_COLOR_RESET, x);
        else if ((j == 0) && (y == 0)) printf("   | " ANSI_COLOR_GREEN "%d " ANSI_COLOR_RESET, x);
        else if ((j == 8) && (y == 1)) printf("| " ANSI_COLOR_CYAN "%d" ANSI_COLOR_RESET " |\n", x);
        else if ((j == 8) && (y == 0)) printf("| " ANSI_COLOR_GREEN "%d" ANSI_COLOR_RESET " |\n", x);
        else if (y == 1) printf("| " ANSI_COLOR_CYAN "%d " ANSI_COLOR_RESET, x);
        else printf("| " ANSI_COLOR_GREEN "%d " ANSI_COLOR_RESET, x);
      }
      a++;
    }
    printf("    --------------------------------------\n");
}

// Display the checked result of the user's grid
void displaycheck(game *g) {
    char a = 'a';
    //printf("\n");
    printf("       1   2   3   4   5   6   7   8   9\n");
    for (size_t i = 0; i < 9; i++) {
      printf("    --------------------------------------\n");
      printf(" %c", a);
      for (size_t j = 0; j < 9; j++) {
        int x = g->grid[i][j];
        int y = g->check[i][j];
        int z = g->comp[i][j];
        if ((j == 0) && (y == 1)) printf("   | " ANSI_COLOR_CYAN "%d " ANSI_COLOR_RESET, x);
        else if ((j == 0) && (x == 0)) printf("   |   ");
        else if ((j == 0) && (y == 0) && (z == 1)) printf("   | " ANSI_COLOR_GREEN "%d " ANSI_COLOR_RESET, x);
        else if ((j == 0) && (y == 0) && (z == 0)) printf("   | " ANSI_COLOR_RED "%d " ANSI_COLOR_RESET, x);
        else if ((j == 8) && (x == 0)) printf("|   |\n");
        else if ((j == 8) && (y == 1)) printf("| " ANSI_COLOR_CYAN "%d" ANSI_COLOR_RESET " |\n", x);
        else if ((j == 8) && (y == 0) && (z == 1)) printf("| " ANSI_COLOR_GREEN "%d" ANSI_COLOR_RESET " |\n", x);
        else if ((j == 8) && (y == 0) && (z == 0)) printf("| " ANSI_COLOR_RED "%d" ANSI_COLOR_RESET " |\n", x);
        else if (x == 0) printf ("|   ");
        else if (y == 1) printf("| " ANSI_COLOR_CYAN "%d " ANSI_COLOR_RESET, x);
        else if (z == 1) printf("| " ANSI_COLOR_GREEN "%d " ANSI_COLOR_RESET, x);
        else printf("| " ANSI_COLOR_RED "%d " ANSI_COLOR_RESET, x);
      }
      a++;
    }
    printf("    --------------------------------------\n");
}

// Ask the player for their move, putting it into the given array.
void ask(char text[100]) {
    printf("Move: ");
    fgets(text, 100, stdin);
}

// Ask the player for the difficulty level, putting it into the given array
void level(char text[100]){
    printf("Level: ");
    fgets(text, 100, stdin);
}

// Ask the player to name the save file to be produced, putting it into the given array
void asksave(char text[100]) {
    printf("Name of save file: ");
    fgets(text, 100, stdin);
}

// Ask the player for the save file to be loaded, putting it into the given array
void askload(char text[100]) {
    printf("Name of load file: ");
    fgets(text, 100, stdin);
}

// Ask the player to choose between a new game and loading a game, putting it into the given array
void asktype(char text[100]) {
    printf("New Game | Load Game: ");
    fgets(text, 100, stdin);
}

// Initializes the level specified by the player, and returns the difficulty
diff sellvl(game *g, char *text) {
    level(text);
    diff d = difficulty(text);
    if (d == Easy){
      printLevel(d);
      newGame(g, d);
      newSGrid(g, d);
      fsquare(g);
      return Easy;
    }
    else if (d == Medium){
      printLevel(d);
      newGame(g, d);
      newSGrid(g, d);
      fsquare(g);
      return Medium;
    }
    else if (d == Hard){
      printLevel(d);
      newSGrid(g, d);
      newGame(g, d);
      fsquare(g);
      return Hard;
    }
    else if (d == Insane){
      printLevel(d);
      newSGrid(g, d);
      newGame(g, d);
      fsquare(g);
      return Insane;
    }
    else {
      printLevel(d);
      return -1;
    }
}

// Calling functions to play the game
void play(game *g){
    char line[100];
    newGame(g, New);
    asktype(line);
    command b = cmd(line);
    diff a = 0;
    while ((b != Exit) || (b != Load) || (b != NewG)) {
      if (b == NewG) {
        diff d = sellvl(g, line);
        a = d;
        while (a == -1) a = sellvl(g, line);
        display(g);
        break;
      }
      else if (b == Load) {
        askload(line);
        loadfile(g, line);
        diff d = loadlevel(g, line);
        //printf("%d\n", d);
        printCommand(b);
        newSGrid(g, d);
        fsquare(g);
        validity v = valid(g, "");
        if(v == BadSave) {
          printInvalid(v);
          exit(0);
          //ask(line);
        }
        else {
          display(g);
          break;
        }
      }
      else if (b == Exit) {
        printCommand(b);
        exit(0);
      }
      else {
        printf(ANSI_COLOR_RED "Type " ANSI_COLOR_RESET "\"new\"" ANSI_COLOR_RED " to start a new game, " ANSI_COLOR_RESET);
        printf("\"load\"" ANSI_COLOR_RED " to load a save file or " ANSI_COLOR_RESET "\"quit\"" ANSI_COLOR_RED " to quit the program\n" ANSI_COLOR_RESET);
        asktype(line);
        b = cmd(line);
      }
    }
    ask(line);
    while (! feof(stdin)) {
      command c = cmd(line);
      validity v = valid(g, line);
      if (c == Solve) {
        printCommand(c);
        if(solve(g, 0, 0)) {
          displaysolved(g);
          ask(line);
        }
        else {
          printf(ANSI_COLOR_RED "No Solution Possible\n" ANSI_COLOR_RESET);
          ask(line);
        }
      }
      else if (c == Reset) {
        printCommand(c);
        newGame(g, a);
        newSGrid(g, a);
        fsquare(g);
        display(g);
        ask(line);
      }
      else if (c == Check) {
        //printCommand(c);
        checkGame(g);
        printCommand(c);
        displaycheck(g);
        ask(line);
      }
      else if (c == Exit) {
        printCommand(c);
        break;
      }
      else if (c == Save) {
        asksave(line);
        savefile(g, line, a);
        printCommand(c);
        display(g);
        ask(line);
      }
      else if (c == Load) {
        askload(line);
        loadfile(g, line);
        diff b = loadlevel(g, line);
        printCommand(c);
        newSGrid(g, b);
        fsquare(g);
        v = valid(g, "");
        if(v == BadSave) {
          printInvalid(v);
          break;
          //ask(line);
        }
        else {
          display(g);
          ask(line);
        }
      }
      else if (v == Level) {
        a = sellvl(g, line);
        while (a == -1) a = sellvl(g, line);
        display(g);
        ask(line);
      }
      else if (v == OK) {
        move(g, row(line), col(line), mov(line[3]));
        display(g);
        ask(line);
      }
      else {
        printf("\n");
        printInvalid(v);
        display(g);
        ask(line);
      }
    }
}

//-----------------------------------------------------------------------------
// Testing

// Tests the newGame function
void testNew(game *g) {
    newGame(g, New);
    assert(g->grid[0][0] == 0);
    assert(g->grid[1][1] == 0);
    assert(g->grid[2][2] == 0);
    assert(g->grid[0][2] == 0);
    assert(g->grid[2][1] == 0);
    newGame(g, Easy);
    assert(g->grid[0][0] == 7);
    assert(g->grid[4][3] == 4);
    assert(g->solved[0][0] == 7);
    assert(g->solved[4][3] == 4);
    newGame(g, Medium);
    assert(g->grid[0][0] == 8);
    assert(g->grid[4][3] == 7);
    assert(g->solved[0][0] == 8);
    assert(g->solved[4][3] == 7);
    newGame(g, Hard);
    assert(g->grid[0][1] == 1);
    assert(g->grid[3][1] == 4);
    assert(g->solved[0][1] == 1);
    assert(g->solved[3][1] == 4);
    newGame(g, Insane);
    assert(g->grid[0][0] == 0);
    assert(g->grid[4][2] == 4);
    assert(g->solved[0][0] == 0);
    assert(g->solved[4][2] == 4);
}

// Tests the row, col and mov function
void testRowColMov() {
    assert(row("a3") == 0);
    assert(row("e7") == 4);
    assert(row("i4") == 8);
    assert(col("d1") == 0);
    assert(col("f6") == 5);
    assert(col("a3") == 2);
    assert(mov('3') == 3);
    assert(mov('7') == 7);
}

// Tests the valid function
void testInvalid(game *g) {
    newGame(g, Easy);
    assert(valid(g, "a1 1") == OK);
    assert(valid(g, "b0 4") == BadDigit);
    assert(valid(g, "2b 1") == BadLetter);
    assert(valid(g, "b2xy 1") == BadFormat);
    assert(valid(g, "b") == BadFormat);
    assert(valid(g, "") == BadFormat);
}

// Tests the cmd function
void testCommand() {
    assert(cmd("quit ") == Exit);
    assert(cmd("reset ") == Reset);
    assert(cmd("exit ") == Exit);
    assert(cmd("clear ") == Reset);
    assert(cmd("solve ") == Solve);
    assert(cmd("uhfaf ") == -1);
    assert(cmd("solved ") == -1);
    assert(cmd("12746 ") == -1);
}

// Tests the difficulty function
void testLevel() {
    assert(difficulty("Easy ") == Easy);
    assert(difficulty("Medium ") == Medium);
    assert(difficulty("Hard ") == Hard);
    assert(difficulty("Insane ") == Insane);
    assert(difficulty("eiofhq ") == -1);
    assert(difficulty("Super ") == -1);
    assert(difficulty("5163 ") == -1);
}
// Tests the fsquare function
void testfsquare(game *g) {
    newGame(g, Easy);
    newSGrid(g, Easy);
    fsquare(g);
    assert(g->check[0][0] == 1);
    assert(g->check[4][4] == 1);
    newGame(g, Medium);
    newSGrid(g, Medium);
    fsquare(g);
    assert(g->check[0][0] == 1);
    assert(g->check[4][3] == 1);
    newGame(g, Hard);
    newSGrid(g, Hard);
    fsquare(g);
    assert(g->check[0][1] == 1);
    assert(g->check[6][4] == 1);
    newGame(g, Insane);
    newSGrid(g, Insane);
    fsquare(g);
    assert(g->check[3][3] == 1);
    assert(g->check[6][0] == 1);
}

// Tests the move function
void testMove(game *g) {
    newGame(g, Easy);
    move(g, row("b2"), col("b2"), mov('1'));
    assert(g->grid[1][1] == 1);
    move(g, row("c2"), col("c2"), mov('7'));
    assert(g->grid[2][1] == 7);
}

// Tests the solve function. The last test is commented out due to it being
// time consuming
void testSolve(game *g) {
    newGame(g, Easy);
    assert(solve(g, 0, 0) == 1);
    newGame(g, Medium);
    assert(solve(g, 0, 0) == 1);
    newGame(g, Hard);
    assert(solve(g, 0, 0) == 1);
    newGame(g, Impossible);
    assert(solve(g, 0, 0) == 0);
    //newGame(g, Insane);
    //assert(solve(g, 0, 0) == 1);
}

// Tests to see if the check function is working correctly
void testCheck(game *g) {
    newGame(g, Easy);
    fsquare(g);
    move(g, 3, 2, 7);
    move(g, 4, 7, 6);
    move(g, 7, 4, 1);
    checkGame(g);
    assert(g->comp[3][2] == 1);
    assert(g->comp[4][7] == 1);
    assert(g->comp[7][4] == 0);
    newGame(g, Medium);
    fsquare(g);
    move(g, 2, 3, 9);
    move(g, 5, 4, 6);
    move(g, 7, 7, 4);
    checkGame(g);
    assert(g->comp[2][3] == 1);
    assert(g->comp[5][4] == 1);
    assert(g->comp[7][7] == 0);
    newGame(g, Hard);
    fsquare(g);
    move(g, 0, 0, 4);
    move(g, 3, 4, 9);
    move(g, 7, 4, 1);
    checkGame(g);
    assert(g->comp[0][0] == 1);
    assert(g->comp[3][4] == 1);
    assert(g->comp[7][4] == 0);
}

// Calls all tests and starts the game if all tests pass
void test() {
    game *g = malloc(sizeof(game));
    testNew(g);
    testRowColMov();
    testInvalid(g);
    testCheck(g);
    testCommand();
    testLevel();
    testfsquare(g);
    testMove(g);
    testSolve(g);
    //printf("All tests passed\n");
    play(g);
}

// Run the program with no args to start the testing, as well as to start the game
int main(int n, char *args[n]) {
    test();
    return 0;
}
